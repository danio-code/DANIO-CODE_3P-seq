# DANIO-CODE_3P-seq


**DANIO-CODE 3P-seq pipeline v1.0**

**I. Introduction:**         
Poly(A)-position profiling (3P-seq) is used to identify 3 UTRs in polyadenylated mRNAs. Briefly, cDNA libraries of the 3' ends of RNAs are generated, and then sequenced from the 3′ end. Two key observations are relevant for data processing: (a) the resulting sequences are antisense to the sample RNA, (b) libraries will be heavily biased towards Ts. 
For a summary of the workflow, see attached diagram. This pipeline also contains adapted portions of a previously published pipeline [1]. 

<p align="center">
<img src="https://gitlab.com/danio-code/DANIO-CODE_3P-seq/raw/3b3952d31994061105e15cef3ffd12c374a34d38/workflow.png"/>
</p>

**II. Dependencies:**    
	Bowtie        
	Samtools     
	Bedtools      
	Fastqc     
	
**III. Workflow:**       
The DANIO-CODE 3P-seq pipeline consists of 6 steps:         
 
Step 1. Filter raw data            
Step 2. Quality Control         
Step 3. Mapping to reference genome       
Step 4. Obtaining Transcription End Sites (TES) from 3P-seq data     
Step 5. Generate Tag Clusters of TES      
Step 6. Create a count matrix for each TES cluster

A more detailed description of each step is provided in the Description section below.       

**IV. Description of the pipeline:**                   

Step 1: The raw data (Fastq file) contains sequences antisense to the sample mRNA, so by design, we expect a bias towards T bases.                   
		Thus:                 
			- reads are reverse complemented              
			- trailing A's are trimmed          
			- remove reads that are < 20bp and contain N's             

Step 2: Run fastqc to check trailing As have been removed                

Step 3: For Bowtie, you will need the reference genome and bowtie index (can be downloaded from igenomes: https://support.illumina.com/sequencing/sequencing_software/igenome.html)                     

Step 4: We keep 3'most coordinate of non-A nucleotide tags                   

Step 5: Finally, tags are clustered with Paraclu     

Step 6: Create a matrix with rows being TES cluster coordinates, and columns sample counts            


**V. How to run the pipeline:**      

1. Filter data:     
`python trim_revcomp.py -q [<fastqfile>] `    
			input = file.fastq   
			output= file_processed.fastq   

2. Quality control:       
`./fastqc.sh [<file_processed.fastq>]`     
			input=file_processed.fastq     
			output=file_processed_fastqc.html, file_processed_fastqc.zip       

3. Mapping reads to genome:
`python alignment_trigger.py -q [<file_processed.fastq>] -g [<genome.fa>] -c [<config.init>]`      
			input= fastq file, reference sequence along with bowtie index, config.init (file contains path to: bowtie, samtools, bedtools. Bowtie paramaters (m=4, v=2, p=20).          
			output= 2 sam files (aligned.sam, filtered.sam). filtered.sam will have bed, bam, bedcount files.     
To obtain the bed file for "aligned.sam" run:         
`./samTobed.sh [<file_processed_aligned>]`  	   			filename should NOT contain .sam!             


4. Obtain Termination End Sites:           
`./bedToTes.sh [<filelist_bedfiles>]`   
			input= filelist.txt (contains paths to all the bed files, filename should NOT contain extension .bed!)       
			output= tes_4Z.bed (sorted BED6 files ready for visualization on zenbu) 
    Pool TES             
`./testoAll.sh [<dir_data>] [<dir_out>]`    
			input= collect all tes_4Z.bed files into dir_data, create a new directory dir_out for output files        
			output= tesPooles.All_danio.bed contains pooled, sorted and grouped TES

5. Generate TES Tag Clusters:            
`./tesToParaclu [<tesPooled.All_danio.bed>]`             
            input= tesPooled.All_danio.bed. Also need: 3pseq.conf           
		    output= par_30_2_200.bed and other files in Paraclu_out folder                

6. Create a count matrix for each TES cluster 
`./bedItes.sh [<filelist_tabcounts>]`                
            input= filelist_tabcounts, also need tabcounts.config             
            output= table_counts.txt                  



**REFERENCES:**    
[1] Lakshmanan, Vairavan et al. “Genome-Wide Analysis of Polyadenylation Events in Schmidtea Mediterranea.” G3: Genes|Genomes|Genetics 6.10 (2016): 3035–3048. PMC. Web. 17 Aug. 2018.               

                                                      
              