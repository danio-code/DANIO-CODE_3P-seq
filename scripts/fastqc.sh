#!/usr/bin/bash


echo 'Loading FastQC'
module load bioinfo-tools
module load FastQC

echo 'fastqc <file>'
file=$1

fastqc $file

