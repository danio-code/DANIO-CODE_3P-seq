#!/bin/bash -l
set -e
#/ Usage: #/ Usage: bedToTes [<filelist>]
#/ filelist = list of file names for the same sample name


module load bioinfo-tools
module load BEDTools/2.27.1

echo "Obtaining Transcription End Sites from 3P-seq data"

filelist=$(<$1)

for mfile in $filelist
do
echo "Current file "${mfile}
echo "BED into minus and positive"
sed '/+/d' $DIR_BED/${mfile}".bed" > $DIR_BED/${mfile}_m.bed
grep + $DIR_BED/${mfile}".bed" > $DIR_BED/${mfile}_p.bed

echo "Keeping 3' coordinates"
cat $DIR_BED/${mfile}_m.bed | awk '{ print $1 "\t" $2-1 "\t" $2 "\t" "\chr" $1 "\:"$2-1 "\-" $2 "\," $6 "\t" $5 "\t" $6}' > $DIR_BED/${mfile}_m_coord.bed
cat $DIR_BED/${mfile}_p.bed | awk '{ print $1 "\t" $3 "\t" $3+1 "\t" "\chr" $1 "\:"$3 "\-" $3+1 "\," $6 "\t" $5 "\t" $6}' > $DIR_BED/${mfile}_p_coord.bed

echo "Sorting Files"
cat $DIR_BED/${mfile}_p_coord.bed | sort -k1,1 -k2,2n > $DIR_BED/${mfile}_p_sorted.bed
cat $DIR_BED/${mfile}_m_coord.bed | sort -k1,1 -k2,2n > $DIR_BED/${mfile}_m_sorted.bed

echo "TES counts: postive strand"
bedtools groupby -i $DIR_BED/${mfile}_p_sorted.bed -g 1,2 -c 6 -o count > $DIR_BED/${mfile}_tesPos

echo "TES counts: negative strand"
bedtools groupby -i $DIR_BED/${mfile}_m_sorted.bed -g 1,3 -c 6 -o count > $DIR_BED/${mfile}_tesNeg

echo "TES output to BED6 format (e.g. for Zenbu upload)"
cat $DIR_BED/${mfile}_tesPos | awk -F "\t" '{print $1"\t"$2"\t"$2+1"\t"""$1":"$2"-"$2",+""\t"$3"\t""+"}' < $DIR_BED/${mfile}_tesPos > $DIR_BED/${mfile}_tesPos_4Z.bed
cat $DIR_BED/${mfile}_tesNeg | awk -F "\t" '{print $1"\t"$2-1"\t"$2"\t"""$1":"$2"-"$2",-""\t"$3"\t""-"}' < $DIR_BED/${mfile}_tesNeg > $DIR_BED/${mfile}_tesNeg_4Z.bed

echo "concatenate strands"
cat $DIR_BED/${mfile}_tesPos_4Z.bed $DIR_BED/${mfile}_tesNeg_4Z.bed > $DIR_TES_COL/${mfile}_tes_4Z.bed


rm $DIR_BED/${mfile}_p.bed
rm $DIR_BED/${mfile}_m.bed
rm $DIR_BED/${mfile}_m_coord.bed
rm $DIR_BED/${mfile}_p_coord.bed
rm $DIR_BED/${mfile}_p_sorted.bed
rm $DIR_BED/${mfile}_m_sorted.bed
rm $DIR_BED/${mfile}_tesPos
rm $DIR_BED/${mfile}_tesNeg
rm $DIR_BED/${mfile}_tesPos_4Z.bed
rm $DIR_BED/${mfile}_tesNeg_4Z.bed

done
