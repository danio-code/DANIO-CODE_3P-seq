             
#!/bin/bash            
set -e          
             
source "3pseq.conf"              
          
echo "tesToParaclu.sh tesPooled.All_danio.bed"                 
            
mfile=$1          
         
          
echo "Filter tes < 0.2TPM"          
alltes=$(awk 'FNR==NR{sum+=$5} END {print sum}' $DIR_TES/$mfile)            
awk -v tesall=$alltes '{if (($5/tesall)*1000000>=0.1) print}' $DIR_TES/$mfile > $DIR_PAR_OUT/tesAll_01_tpm_new.bed        
            
echo "BED into minus and positive"            
sed '/+/d' $DIR_PAR_OUT/tesAll_01_tpm_new.bed > $DIR_PAR_OUT/allm_4Z.bed              
grep + $DIR_PAR_OUT/tesAll_01_tpm_new.bed > $DIR_PAR_OUT/allp_4Z.bed               
           
            
echo "TES output to paraclu format"             
awk -F "\t" '{print$1"\t"$6"\t"$2"\t"$5}' < $DIR_PAR_OUT/allp_4Z.bed > $DIR_PAR_OUT/tes_pos_4P             
awk -F "\t" '{print$1"\t"$6"\t"$3"\t"$5}' < $DIR_PAR_OUT/allm_4Z.bed > $DIR_PAR_OUT/tes_neg_4P           
           
echo "Sorting the pos prior paraclu clustering"           
sort -k1,1 -k3n $DIR_PAR_OUT/tes_pos_4P > $DIR_PAR_OUT/tes_pos_4Ps           
           
echo "Sorting the neg prior paraclu clustering"             
sort -k1,1 -k3n $DIR_PAR_OUT/tes_neg_4P > $DIR_PAR_OUT/tes_neg_4Ps           
           
echo "Paraclu clustering"          
$DIR_PAR/./paraclu 30 $DIR_PAR_OUT/tes_pos_4P > $DIR_PAR_OUT/par_pos_30          
$DIR_PAR/./paraclu 30 $DIR_PAR_OUT/tes_neg_4Ps > $DIR_PAR_OUT/par_neg_30           
         
echo "Paraclu-cut"           
$DIR_PAR/paraclu-cut.sh -d 2 -l 200  $DIR_PAR_OUT/par_pos_30 > $DIR_PAR_OUT/par_pos_30_2_200          
$DIR_PAR/paraclu-cut.sh -d 2 -l 200  $DIR_PAR_OUT/par_neg_30 > $DIR_PAR_OUT/par_neg_30_2_200         
           
echo "Combined clusters and create BED file"          
cat $DIR_PAR_OUT/par_pos_30_2_200 $DIR_PAR_OUT/par_neg_30_2_200 > $DIR_PAR_OUT/par_30_2_200            
awk -F "\t" '{print$1"\t"$3"\t"$4"\t"$1":"$3".."$4","$2"\t"$6"\t"$2}' < $DIR_PAR_OUT/par_30_2_200 > $DIR_PAR_OUT/par_30_2_200.bed           
              
echo "No of clusters paraclu 30: positive strand"           
awk '{print$1}' < $DIR_PAR_OUT/par_pos_30 | wc -l          
        
echo "No of clusters paraclu 30: negative strand"          
awk '{print$1}' < $DIR_PAR_OUT/par_neg_30 | wc -l        
                                                  
echo "No of clusters paraclu 30 2 200: positive strand"        
awk '{print$1}' < $DIR_PAR_OUT/par_pos_30_2_200 | wc -l          
         
echo "No of clusters paraclu 30 2 200: negative strand"          
awk '{print$1}' < $DIR_PAR_OUT/par_neg_30_2_200 | wc -l           
              
echo "DONE"             
