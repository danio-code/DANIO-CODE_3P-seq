#!/bin/bash -l
#/ Usage: bedItes [<filelist_tabcounts>]
#/ filelist_tabcounts = list of file names for all samples
set -e

source 'tabcounts.config'

module load bioinfo-tools
module load BEDTools
module list

filelist=$(<$1)
echo filelist
printf "%s" "coordinates" > $DIR_OUT/$TAB_NAME
for mfile in $filelist
do

    STRING="File in progress: "${mfile}
    echo $STRING
    intersectBed -a 3P-seq/Paraclu_out/par_30_2_200.bed -b 3P-seq/tes_4Z_files/${mfile}_tes_4Z.bed -loj -s > $DIR_BEDITES/${mfile}_counts_fortab
    echo "TES intersected BED done"

    echo "file processing"
    bedtools groupby -i $DIR_BEDITES/${mfile}_counts_fortab -g 1,2,3,4,6 -c 11 -o sum >$DIR_BEDITES/${mfile}_counts1_fortab
    awk -v OFS='\t' '{if($6=="-1") $6=0; print $6 }' $DIR_BEDITES/${mfile}_counts1_fortab > $DIR_BEDITES/${mfile}_counts

    rm $DIR_BEDITES/${mfile}_counts_fortab
    rm $DIR_BEDITES/${mfile}_counts1_fortab

    printf "\t%s" "${mfile}" >> $DIR_OUT/$TAB_NAME
    echo "Done"
done
echo "Creating the count matrix"
printf "\n" >> $DIR_OUT/$TAB_NAME
awk '{ print $4}' 3P-seq/Paraclu_out/par_30_2_200.bed > paraclu_coordinates.bed
paste -d "\t"  paraclu_coordinates.bed $DIR_BEDITES/* >> $DIR_OUT/$TAB_NAME
echo "Done"
