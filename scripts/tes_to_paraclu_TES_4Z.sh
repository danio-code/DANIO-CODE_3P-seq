             
#!/bin/bash            
set -e          
        
# this should be run on file_tes_4Z.bed files 

#$DIR_TES= dir where [<file_tes_4Z.bed>] is        
#$DIR_PAR_OUT= create a dir for Paraclu Output          
#$DIR_PAR = dir of Paraclu-9     
             
DIR_TES="/proj/uppstore2017255/private/DANIO-CODE/3UTR/3P-seq/DCD000360SR/ALIGNED_ALL_READS"            
DIR_PAR_OUT="/proj/uppstore2017255/private/DANIO-CODE/3UTR/3P-seq/DCD000360SR/ALIGNED_ALL_READS/Paraclu_out/"         
DIR_PAR="/proj/uppstore2017255/private/DANIO-CODE/3UTR/DANIO-CODE_3P-seq/scripts/paraclu-9"           
          
echo "tesToParaclu.sh file_tes_4Z.bed"                 
            
mfile=$1          
         
                      
echo "BED into minus and positive"            
sed '/+/d' $mfile > $DIR_PAR_OUT/m_4Z.bed              
grep + $mfile > $DIR_PAR_OUT/p_4Z.bed               
           
            
echo "TES output to paraclu format"             
awk -F "\t" '{print$1"\t"$6"\t"$2"\t"$5}' < $DIR_PAR_OUT/p_4Z.bed > $DIR_PAR_OUT/tes_pos_4P             
awk -F "\t" '{print$1"\t"$6"\t"$3"\t"$5}' < $DIR_PAR_OUT/m_4Z.bed > $DIR_PAR_OUT/tes_neg_4P           
           
echo "Sorting the pos prior paraclu clustering"           
sort -k1,1 -k3n $DIR_PAR_OUT/tes_pos_4P > $DIR_PAR_OUT/tes_pos_4Ps           
           
echo "Sorting the neg prior paraclu clustering"             
sort -k1,1 -k3n $DIR_PAR_OUT/tes_neg_4P > $DIR_PAR_OUT/tes_neg_4Ps           
           
echo "Paraclu clustering"          
$DIR_PAR/paraclu.dms  30 $DIR_PAR_OUT/tes_pos_4P > $DIR_PAR_OUT/par_pos_30          
$DIR_PAR/paraclu.dms 30 $DIR_PAR_OUT/tes_neg_4Ps > $DIR_PAR_OUT/par_neg_30           
         
echo "Paraclu-cut"           
$DIR_PAR/paraclu-cut.sh -d 2 -l 200  $DIR_PAR_OUT/par_pos_30 > $DIR_PAR_OUT/par_pos_30_2_200          
$DIR_PAR/paraclu-cut.sh -d 2 -l 200  $DIR_PAR_OUT/par_neg_30 > $DIR_PAR_OUT/par_neg_30_2_200         
           
echo "Combined clusters and create BED file"          
cat $DIR_PAR_OUT/par_pos_30_2_200 $DIR_PAR_OUT/par_neg_30_2_200 > $DIR_PAR_OUT/par_30_2_200            
awk -F "\t" '{print$1"\t"$3"\t"$4"\t"$1":"$3".."$4","$2"\t"$6"\t"$2}' < $DIR_PAR_OUT/par_30_2_200 > $DIR_PAR_OUT/par_30_2_200.bed           
              
echo "No of clusters paraclu 30: positive strand"           
awk '{print$1}' < $DIR_PAR_OUT/par_pos_30 | wc -l          
        
echo "No of clusters paraclu 30: negative strand"          
awk '{print$1}' < $DIR_PAR_OUT/par_neg_30 | wc -l        
                                                  
echo "No of clusters paraclu 30 2 200: positive strand"        
awk '{print$1}' < $DIR_PAR_OUT/par_pos_30_2_200 | wc -l          
         
echo "No of clusters paraclu 30 2 200: negative strand"          
awk '{print$1}' < $DIR_PAR_OUT/par_neg_30_2_200 | wc -l           
              
echo "DONE"  