#!/usr/bin/bash

echo 'Loading samtools, BEDTools'

module load bioinfo-tools
module load samtools/1.8
module load BEDTools/2.27.1

echo 'Usage: bash samTobed.sh [<filename>]'

filename=$1
  
echo "Converting SAM to BAM"
samtools view -bS ${filename}.sam -o ${filename}.bam

echo "Converting BAM to BED"
bamToBed -i ${filename}.bam > ${filename}.bed

