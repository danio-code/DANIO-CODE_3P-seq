#!/bin/bash

echo "tesToAll.sh dir_data dir_out"

module load bioinfo-tools
module load BEDTools/2.27.1

DIR_DATA=$1
DIR_OUT=$2

pTes=tesPooled

echo "Pooling TES"
cat $DIR_DATA/*_tes_4Z.bed > $DIR_OUT/$pTes

echo "Sorting pooled TES"
sort -k 1,1 -k 2,2n -k 3,3n -k 6,6 $DIR_OUT/$pTes > $DIR_OUT/$pTes.Sorted

echo "Grouping and adding TES"
bedtools groupby -i $DIR_OUT/$pTes.Sorted -g 1,2,3,6 -c 5 -o sum > $DIR_OUT/$pTes.SortedGrouped

echo "Formating file"
awk -F "\t" '{print$1"\t"$2"\t"$3"\t"$1":"$2"-"$3"\t"$5"\t"$4}' < $DIR_OUT/$pTes.SortedGrouped > $DIR_OUT/$pTes.All_danio.bed

